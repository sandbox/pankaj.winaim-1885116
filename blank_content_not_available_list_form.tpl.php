<p>
<div style="float:right;">
	<?php
	    $destData= array();		
		print l(t('<< Go Back'), $back_url, array('query' => $destData['destination'])); 
		
	?>
</div>
</p>
<p>
	<table>
			<tr>
			    <th><b> Title </b></th>
				<th><b> Content Name </b></th>
				<th><b> Fields </b></th>
				<th>
					<b> Actions </b></th>
	        </tr>
	
	
		<?php 
            if(count($field_alias) > 0){
			   foreach ($field_alias as $nid => $data): ?>
				<tr>
					<td>
						<?php print $data['title'];?>
					</td>
					
					<td>
					    <?php print arg(3);?>
					</td>
					
					<td>
					    <?php print arg(4);?>
					</td>
					<td>
					    <?php print l(t('Edit'), 'node/'.$nid.'/edit', array('query' => array('destination' => $data['destination'])));?>
					</td>					
				</tr>
			<?php endforeach;
			}else{
			?>
			<tr>
					<td colspan="4"><span class="blank_message">No blank records found for this fields.</span>			
					</td>
			</tr>		
			<?php } ?>
	</table>
</p>
